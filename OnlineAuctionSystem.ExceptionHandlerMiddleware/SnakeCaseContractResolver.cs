﻿using Newtonsoft.Json.Serialization;

namespace OnlineAuctionSystem.ExceptionHandlerMiddleware
{
    public class SnakeCaseContractResolver: DefaultContractResolver
    {
        public SnakeCaseContractResolver()
        {
            NamingStrategy = new SnakeCaseNamingStrategy
            {
                ProcessDictionaryKeys = true,
                OverrideSpecifiedNames = true
            };
        }
    }
}