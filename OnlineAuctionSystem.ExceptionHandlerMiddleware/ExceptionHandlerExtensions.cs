﻿using Microsoft.AspNetCore.Builder;

namespace OnlineAuctionSystem.ExceptionHandlerMiddleware
{
    public static class ExceptionHandlerExtensions
    {
        public static IApplicationBuilder UseExceptionHandlerMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}