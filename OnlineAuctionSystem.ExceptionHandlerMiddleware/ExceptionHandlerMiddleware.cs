﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OnlineAuctionSystem.Entities.Exceptions;


namespace OnlineAuctionSystem.ExceptionHandlerMiddleware
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly JsonSerializer _snakeSerializer = JsonSerializer.CreateDefault(new JsonSerializerSettings
            {ContractResolver = new SnakeCaseContractResolver()});

        public static readonly JsonResult UnknownServerError = new JsonResult(
            new {Error = "unknown_server_error", Reason = "Неизвестная ошибка"}) {StatusCode = 500};

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        
        public async Task InvokeAsync(HttpContext context)
        {
            JsonResult exceptionResult = null;

            try
            {
                await _next(context).ConfigureAwait(true);
            }
            catch (FluentValidation.ValidationException ex)
            {
                var resEx = new JsonValidationException(ex.Errors);
                _logger.LogError(resEx.ToString());
                exceptionResult = resEx.ToJson();
            }
            catch (FinalException ex)
            {
                _logger.LogError(ex.ToString());
                exceptionResult = ex.ToJson();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception.ToString());
                exceptionResult = UnknownServerError;
            }

            if (exceptionResult != null)
            {
                context.Response.StatusCode = exceptionResult.StatusCode ?? 500;
                context.Response.ContentType = "application/json; charset=utf-8";
                string body;
                await using (var stringWriter = new StringWriter())
                {
                    _snakeSerializer.Serialize(stringWriter, exceptionResult.Value);
                    body = stringWriter.ToString();
                }
                await context.Response.WriteAsync(body).ConfigureAwait(true);
            }
        }
    }
}